package com.amh.monitormanagement817;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonitorManagement817Application {

    public static void main(String[] args) {
        SpringApplication.run(MonitorManagement817Application.class, args);
    }

}
