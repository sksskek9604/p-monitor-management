package com.amh.monitormanagement817.configure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Collections;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    private String version;
    private String title;
    private final String TITLE_FIX = "모니터 관리 API ";
    // final이기 때문에 바뀌지 않겠다는 것이고 대문자인 이유는 상수이기 때문이다.
    // 그래서 미리 " " 안에 값을 넣어준 것임.

    // Docket은 다운받은 스프링폭스에서 제공해주는 타입임
    // version1 API를 작성해주는 문서를 만들어줄 거기 때문에 "V1"
    // 차량관리API 뒤에 한칸 띄어서 공백만들어주면 더 깔끔함.
    @Bean
    public Docket apiV1() {
        version = "V1";
        title = TITLE_FIX + version;

        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .groupName(version)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.amh.monitormanagement817"))
                .paths(PathSelectors.ant("/v1/**"))
                .build()
                .apiInfo(getApiInfo(title, version))
                .securitySchemes(Collections.singletonList(getApiKey()))
                .enable(true);

    }
    @Bean
    public Docket apiV2() {
        version = "V2";
        title = TITLE_FIX + version;

        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .groupName(version)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.amh.monitormanagement817"))
                .paths(PathSelectors.ant("/v2/**"))
                .build()
                .apiInfo(getApiInfo(title, version))
                .securitySchemes(Collections.singletonList(getApiKey()))
                .enable(true);

    }

    private ApiInfo getApiInfo(String title, String version) {
        return new ApiInfo(
                title,
                "Swagger API Docs",
                version,
                "sksskek.com",
                new Contact("amh", "sksskek.com", "sksskek9604@gmail.com"),
                "Licenses",
                "sksskek.com",
                new ArrayList<>()
        );
    }

    private ApiKey getApiKey() {
        return new ApiKey("jwtToken", "X-AUTH-TOKEN", "header");
    }
}
