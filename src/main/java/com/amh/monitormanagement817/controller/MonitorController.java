package com.amh.monitormanagement817.controller;

import com.amh.monitormanagement817.model.*;
import com.amh.monitormanagement817.service.MonitorService;
import com.amh.monitormanagement817.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "모니터 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/monitor")
public class MonitorController {

    private final MonitorService monitorService;

    @ApiOperation(value = "모니터 정보 등록")
    @PostMapping("/new")
    public CommonResult setMonitor(@RequestBody @Valid MonitorRequest monitorRequest) {
        monitorService.setMonitor(monitorRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "모니터 정보 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "모니터 시퀀스", required = true)})
    @GetMapping("/{id}")
    public SingleResult<MonitorItem> getMonitor(@PathVariable long id) {
        return ResponseService.getSingleResult(monitorService.getMonitor(id));
    }

    @ApiOperation(value = "모니터 정보 리스트")
    @GetMapping("/all")
    public ListResult<MonitorItem> getMonitors() {
        return ResponseService.getListResult(monitorService.getMonitors(), true);
    }

    @ApiOperation(value = "모니터 정보 수정하기")
    @PutMapping("/{id}")
    public CommonResult putMonitor(@PathVariable long id, @RequestBody @Valid MonitorRequest monitorRequest) {
        monitorService.putMonitor(id, monitorRequest);
        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "모니터 정보 삭제하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "모니터 시퀀스", required = true)})
    @DeleteMapping("/{id}")
    public CommonResult delMonitor(@PathVariable long id) {
        monitorService.delMonitor(id);
        return ResponseService.getSuccessResult();
    }
}
