package com.amh.monitormanagement817.entity;

import com.amh.monitormanagement817.enums.Color;
import com.amh.monitormanagement817.enums.MonitorCompany;
import com.amh.monitormanagement817.interfaces.CommonModelBuilder;
import com.amh.monitormanagement817.model.MonitorRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Monitor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 30)
    private MonitorCompany companyName;

    @Column(nullable = false)
    private Float sizeMonitor;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 30)
    private Color colorMonitor;

    @Column(nullable = false)
    private LocalDate datePurchase;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putMonitor(MonitorRequest monitorRequest) {
        this.companyName = monitorRequest.getCompanyName();
        this.sizeMonitor = monitorRequest.getSizeMonitor();
        this.colorMonitor = monitorRequest.getColorMonitor();
        this.datePurchase = monitorRequest.getDatePurchase();
        this.dateUpdate = LocalDateTime.now();
    }

    private Monitor(MonitorBuilder builder) {
        this.companyName = builder.companyName;
        this.sizeMonitor = builder.sizeMonitor;
        this.colorMonitor = builder.colorMonitor;
        this.datePurchase = builder.datePurchase;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;

    }
    public static class MonitorBuilder implements CommonModelBuilder<Monitor> {
        private final MonitorCompany companyName;
        private final Float sizeMonitor;
        private final Color colorMonitor;
        private final LocalDate datePurchase;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public MonitorBuilder(MonitorRequest monitorRequest) {
            this.companyName = monitorRequest.getCompanyName();
            this.sizeMonitor = monitorRequest.getSizeMonitor();
            this.colorMonitor = monitorRequest.getColorMonitor();
            this.datePurchase = monitorRequest.getDatePurchase();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public Monitor build() {
            return new Monitor(this);
        }
    }

}
