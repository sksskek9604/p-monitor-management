package com.amh.monitormanagement817.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Color {
    WHITE("화이트"),
    BLACK("블랙"),
    PINK("분홍"),
    BLUE("블루"),
    ETC("그외");

    private final String name;
}
