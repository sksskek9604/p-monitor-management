package com.amh.monitormanagement817.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MonitorCompany {
    KEUMBEE("금비전자"),
    DYCNI("대연씨앤아이"),
    DIK("디아이케이"),
    SDN("에스디엔컴퍼니"),
    HEX("헥스파워"),
    ETC("그외");

    private final String name;
}
