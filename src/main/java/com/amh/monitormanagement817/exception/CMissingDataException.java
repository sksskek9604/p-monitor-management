package com.amh.monitormanagement817.exception;

public class CMissingDataException extends RuntimeException {
    // 1번, 생성자 3개만들 것임
    public CMissingDataException(String msg, Throwable t) {
        super(msg, t); //위에 두개 받아줄 것.
    }

    public CMissingDataException(String msg) {
        super(msg);
    }

    // 3번째는 빈바구니
    public CMissingDataException() {
        super();
    }
}
