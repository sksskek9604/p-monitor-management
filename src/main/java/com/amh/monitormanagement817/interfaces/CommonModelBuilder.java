package com.amh.monitormanagement817.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
