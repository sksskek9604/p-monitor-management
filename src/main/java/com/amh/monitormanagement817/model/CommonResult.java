package com.amh.monitormanagement817.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommonResult {
    private Boolean isSuccess; //1번
    private Integer code; //2번 이넘에 쓰였던 자료형
    private String msg; //3번 msg = 메세지임

}
