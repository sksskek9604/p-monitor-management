package com.amh.monitormanagement817.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ListResult<T> extends CommonResult {
    private List<T> list; //list는 여러개다보니, 게시판 생각해보면 '몇번째' 페이지인지, '총 몇페이지'인지가 있겠죠?

    private Long totalItemCount; //총 몇개인지 세는 것이란걸 알 수 있음. 엄청 많을 수 있으니 가장 큰 Long 자료형

    private Integer totalPage; //총 페이지 수, 백만번째 페이지가 있을리는 없으니 Integer

    private Integer currentPage; //현재 내가 몇페이지에 있는지


}
