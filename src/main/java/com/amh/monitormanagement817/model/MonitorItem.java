package com.amh.monitormanagement817.model;

import com.amh.monitormanagement817.entity.Monitor;
import com.amh.monitormanagement817.enums.MonitorCompany;
import com.amh.monitormanagement817.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MonitorItem {

    @ApiModelProperty(notes = "모니터 시퀀스", required = true)
    private Long id;

    @ApiModelProperty(notes = "제조사명(~30자)", required = true)
    @Enumerated(value = EnumType.STRING)
    private MonitorCompany companyName;

    @ApiModelProperty(notes = "모니터 사이즈", required = true)
    private String sizeMonitor;

    @ApiModelProperty(notes = "모니터 색상", required = true)
    @Enumerated(value = EnumType.STRING)
    private String colorMonitor;

    @ApiModelProperty(notes = "구매 날짜", required = true)
    private LocalDate datePurchase;

    @ApiModelProperty(notes = "생성 날짜", required = true)
    private LocalDateTime dateCreate;

    @ApiModelProperty(notes = "수정 날짜", required = true)
    private LocalDateTime dateUpdate;
    private MonitorItem(MonitorItemBuilder monitorItemBuilder) {

        this.id = monitorItemBuilder.id;
        this.companyName = monitorItemBuilder.companyName;
        this.sizeMonitor = monitorItemBuilder.sizeMonitor;
        this.colorMonitor = monitorItemBuilder.colorMonitor;
        this.datePurchase = monitorItemBuilder.datePurchase;
        this.dateCreate = monitorItemBuilder.dateCreate;
        this.dateUpdate = monitorItemBuilder.dateUpdate;

    }

    public static class MonitorItemBuilder implements CommonModelBuilder<MonitorItem> {

        private final Long id;
        private final MonitorCompany companyName;
        private final String sizeMonitor;
        private final String colorMonitor;
        private final LocalDate datePurchase;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public MonitorItemBuilder(Monitor monitor) { // 재가공 단계 작성 하는 곳 위에 자료형 타입도 바꿔줘야함
            this.id = monitor.getId();
            this.companyName = monitor.getCompanyName();
            this.sizeMonitor = monitor.getSizeMonitor() + "inch";
            this.colorMonitor = monitor.getColorMonitor().getName();
            this.datePurchase = monitor.getDatePurchase();
            this.dateCreate = monitor.getDateCreate();
            this.dateUpdate = monitor.getDateUpdate();
        }
        @Override
        public MonitorItem build() {
            return new MonitorItem(this);
        }
    }


}
