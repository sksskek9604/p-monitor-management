package com.amh.monitormanagement817.model;

import com.amh.monitormanagement817.enums.Color;
import com.amh.monitormanagement817.enums.MonitorCompany;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MonitorRequest {

    @ApiModelProperty(notes = "제조사명(~30자)", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private MonitorCompany companyName;

    @ApiModelProperty(notes = "모니터 사이즈", required = true)
    @NotNull
    private Float sizeMonitor;

    @ApiModelProperty(notes = "모니터 색상", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Color colorMonitor;

    @ApiModelProperty(notes = "구매 날짜", required = true)
    @NotNull
    private LocalDate datePurchase;

}
