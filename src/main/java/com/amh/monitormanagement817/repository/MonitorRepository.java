package com.amh.monitormanagement817.repository;

import com.amh.monitormanagement817.entity.Monitor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MonitorRepository extends JpaRepository<Monitor, Long> {
}
