package com.amh.monitormanagement817.service;

import com.amh.monitormanagement817.model.ListResult;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ListConvertService {
    public static PageRequest getPageAble(int pageNum) {//몇번째 페이지를 가지고 오고 싶은지?
        return PageRequest.of(pageNum - 1, 10);

    }

    public static PageRequest getPageAble(int pageNum, int pageSize) {
        return PageRequest.of(pageNum - 1, pageSize);
    }

    public static <T> ListResult<T> settingResult(List<T> list) {
        ListResult<T> result = new ListResult<>();
        result.setList(list);
        result.setTotalItemCount((long)list.size()); //리스트가 총 몇개인지 , long 형태도 함께 넣어줌
        result.setTotalPage(1);
        result.setCurrentPage(1);
        // 리스트 통채로 가져와서 통으로 퉁치겠다. = 하나로 처리하겠다.
        // 페이지는 한페이지라는 뜻이 되는 것, 그리고 현재페이지도 마찬가지로 1페이지가 되는 것.

        return result;
    }
    // 모델에 있는 4개 다 받을 것
    public static <T> ListResult<T> settingResult(List<T> list, long tatalItemCount, int totalPage, int currentPage) {
        ListResult<T> result = new ListResult<>();
        result.setList(list);
        result.setTotalItemCount(tatalItemCount);
        result.setTotalPage(totalPage == 0 ? 1 : totalPage); //(if문과 똑같은 것) 0페이지에 있다면 1이다.
        result.setCurrentPage(currentPage + 1); //만약 내가 0페이지에 있다해도, 창에서는 0이라고 창에 뜨는게 아니라 1페이지가 뜨죠?

        return result;
    }
}

