package com.amh.monitormanagement817.service;

import com.amh.monitormanagement817.entity.Monitor;
import com.amh.monitormanagement817.exception.CMissingDataException;
import com.amh.monitormanagement817.model.ListResult;
import com.amh.monitormanagement817.model.MonitorItem;
import com.amh.monitormanagement817.model.MonitorRequest;
import com.amh.monitormanagement817.repository.MonitorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MonitorService {

    private final MonitorRepository monitorRepository;

    public void setMonitor(MonitorRequest monitorRequest) {
        Monitor monitor = new Monitor.MonitorBuilder(monitorRequest).build();
        monitorRepository.save(monitor);
    }

    public MonitorItem getMonitor(long id) {
        Monitor monitor = monitorRepository.findById(id).orElseThrow(CMissingDataException::new);
        MonitorItem monitorItem = new MonitorItem.MonitorItemBuilder(monitor).build();

        return monitorItem; // 색깔 alt+enter
    }

    public ListResult<MonitorItem> getMonitors() {
        List<MonitorItem> result = new LinkedList<>();

        List<Monitor> monitors = monitorRepository.findAll();

        for (Monitor monitor : monitors) {
            MonitorItem monitorItem = new MonitorItem.MonitorItemBuilder(monitor).build();
            result.add(monitorItem);
        }
        return ListConvertService.settingResult(result);
    }

    public void putMonitor(long id, MonitorRequest monitorRequest) {
        Monitor monitor = monitorRepository.findById(id).orElseThrow(CMissingDataException::new);
        monitor.putMonitor(monitorRequest);
        monitorRepository.save(monitor);
    }

    public void delMonitor(long id) {
        monitorRepository.deleteById(id);
    }

}
